import java.util.Scanner;
public class Roulette {
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    int playerAmount = 0;
    int betAmount = 0;
    int number = 0;
    int wheelNumber = 0;
    String answer = null;
    
    System.out.println("How much would you like to bet?");
    playerAmount = input.nextInt();
    System.out.println("Do you want to make a bet?(y/n)");
    answer = input.next();
    while (answer.equals("y")) {
      System.out.println("What number do you want to bet on? (0 to 36)");
      number = input.nextInt();
      System.out.println("How much do you like to bet");
      betAmount = input.nextInt();
      // call methods to receive a random number between 0 and 36
      wheelNumber = getWheelNumber();
      // calcuate player amount for win or lost
      playerAmount += CalculatePlayerAmount(wheelNumber, number, betAmount);
      System.out.println("You still have $" + playerAmount + " to bet with");
      // ask player if they want to continue betting
      System.out.println("Do you want to make a bet?(y/n)");
      answer = input.next();
    }
    System.out.println("You end the Roulette Game. Thank you for playing");
  }

  public static int getWheelNumber() {
    RouletteWheel wheel = new RouletteWheel();
    wheel.spin();
    System.out.println("Wheel number is " + wheel.getValue());
    return wheel.getValue();
  }

  public static int CalculatePlayerAmount(int wheelNumber, int number, int betAmount) {
    // if player win, return payout amount
    if (number == wheelNumber) {
      // if player win bet, player receive $35 for every $1
      final int ODDS = 35;
      int payoutBet = ODDS * betAmount;
      System.out.println("You won $" + payoutBet);
      return payoutBet;
      // if player lose, return player's lost
    } else {
      System.out.println("You lost $" + betAmount);
      return -betAmount;
    }
  }
}
